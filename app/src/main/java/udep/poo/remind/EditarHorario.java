package udep.poo.remind;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class EditarHorario extends AppCompatActivity implements OnClickListener {
    private static final String TAG = "Editar";
    Data eventNameData;
    private Button guardar;
    private Button cerrar;
    private Spinner spinnerHora;
    private EditText editEvento;

    public EditarHorario(){

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        guardar = (Button) findViewById(R.id.buttonGuardar);
        cerrar = (Button) findViewById(R.id.buttonCerrar);
        guardar.setOnClickListener(this);
        cerrar.setOnClickListener(this);
        editEvento = (EditText) findViewById(R.id.editEvento);
        spinnerHora = (Spinner) findViewById(R.id.spinnerHora);
        eventNameData = new Data(this);
        String [] opciones = {"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24"};
        ArrayAdapter <String> horas =  new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, opciones);
        spinnerHora.setAdapter(horas);
    }

    @Override
    public void onClick(View view){
        if (view.getId() == cerrar.getId()){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }else if (view.getId() == guardar.getId()){
            if (editEvento.length()!=0) {
                    String eventName = editEvento.getText().toString();
                    String eventHour = spinnerHora.getSelectedItem().toString().trim();
                    AddData(eventName, eventHour);
                    Intent intent = new Intent(this, MainActivity.class);
                    startActivity(intent);
                }else{
                    toastAlert("Ha ingresado mal los datos");
                }
            }else{
                toastAlert("Ha ingresado mal los datos");
        }
    }

    public void AddData (String newEventN, String newEventH){
        boolean insertEN = eventNameData.addData(newEventN, newEventH);
        if (insertEN) {
            toastAlert("La data se agregó");
        }else{
            toastAlert("Ha ocurrido un error");
        }
    }


    private  void toastAlert(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

}
