package udep.poo.remind;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class Data extends SQLiteOpenHelper{
    private static final String TAG = "Data";
    private static final String TABLE_NAME = "Horario";
    private static final String COL1 = "ID";
    private static final String COL2 = "name";
    private static final String COL3 = "hour";

    public Data(Context context){
        super(context,TABLE_NAME,null,1);
    }
    @Override
    public void onCreate(SQLiteDatabase db){
        String createTable = "CREATE TABLE " + TABLE_NAME + "(ID INTEGER PRIMARY KEY AUTOINCREMENT, "+COL2+" TEXT, "+COL3+" INTEGER)";
        db.execSQL(createTable);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db,int i, int i1){
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }
    public boolean addData(String nameEvent, String hourEvent){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues event = new ContentValues();
        event.put(COL2, nameEvent);
        event.put(COL3, hourEvent);
        Log.d(TAG, "addData: Adding "+nameEvent+" to "+TABLE_NAME);
        long result = db.insert(TABLE_NAME, null, event);
        if (result == -1){
            return false;
        }else{
            return true;
        }
    }
    public Cursor getData(){
        String query = "SELECT * FROM "+TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor data = db.rawQuery(query,null);
        return data;
    }
    public Cursor getItemID(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT "+COL1+" FROM "+TABLE_NAME+" WHERE "+COL2+" = "+name+"'";
        Cursor data = db.rawQuery(query,null);
        return data;
    }
    public void updateEvent(String newEvent,int i,String oldEvent){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE "+TABLE_NAME+" SET "+COL2+" = '"+newEvent+"' WHERE "+COL1+" = '"+i+"'"+" AND "+
                COL2+" = '"+oldEvent+"'";
        Log.d(TAG,"updateEvent: query: "+query);
        Log.d(TAG,"updateEvent: El nombre cambia a "+newEvent);
        db.execSQL(query);
    }
    public void deleteEvent(int i,String Event) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM "+TABLE_NAME+" WHERE "+COL1+" = '"+i+"'"+" AND "+COL2+" = '"+Event+"'";
        Log.d(TAG,"deleteEvent: query: "+query);
        Log.d(TAG,"deleteEvent: "+Event+" se está borrando de la base de datos");
        db.execSQL(query);
    }
}

