package udep.poo.remind;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;


public class Horario extends AppCompatActivity implements OnClickListener {
    private static final String TAG = "Horario";
    Data event;
    RecyclerView recyclerView;
    private Button cerrar;
    ArrayList<String> nameData, hourData;
    VistaLista vistaLista;
    public Horario(){
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_horario);
        cerrar = (Button) findViewById(R.id.buttonCerrar);
        cerrar.setOnClickListener(this);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        event = new Data(this);
        nameData = new ArrayList<>();
        hourData = new ArrayList<>();
        populateListView();
        vistaLista = new VistaLista(this, nameData,hourData);
        recyclerView.setAdapter(vistaLista);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
    @Override
    public void onClick(View view){
        if (view.getId() == cerrar.getId()){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }
    private void populateListView(){
        Log.d(TAG,"populateListView: Presentando data en la lista");
        Cursor dataN = event.getData();
        while (dataN.moveToNext()){
            nameData.add(dataN.getString(1));
            hourData.add(dataN.getString(2));
        }
    }
}