package udep.poo.remind;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class VistaLista extends RecyclerView.Adapter<VistaLista.Vistamejor> {
    Context context;
    ArrayList nombreArray, horaArray;
    VistaLista(Context context, ArrayList nombre, ArrayList hora){
        this.context=context;
        this.nombreArray=nombre;
        this.horaArray=hora;
    }

    @NonNull
    @Override
    public Vistamejor onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.my_list,parent,false);
        return new Vistamejor(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Vistamejor holder, int position) {
        holder.nombre.setText(String.valueOf(nombreArray.get(position)));
        holder.hora.setText(String.valueOf(horaArray.get(position)));
    }

    @Override
    public int getItemCount() {
        return nombreArray.size();
    }
    public class Vistamejor extends RecyclerView.ViewHolder {
        TextView nombre, hora;
        public Vistamejor(@NonNull View itemView) {
            super(itemView);
            nombre = itemView.findViewById(R.id.textNombre);
            hora = itemView.findViewById(R.id.textHora);
        }
    }
}
