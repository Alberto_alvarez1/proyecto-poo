package udep.poo.remind;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements OnClickListener {
    private Button editar;
    private Button horario;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editar = (Button) findViewById(R.id.buttonEditar);
        horario = (Button) findViewById(R.id.buttonHorario);
        editar.setOnClickListener(this);
        horario.setOnClickListener(this);
    }

    @Override
    public void onClick(View view){
        if (view.getId() == editar.getId()){
            Intent intent = new Intent(this, EditarHorario.class);
            startActivity(intent);
        }else if (view.getId() == horario.getId()){
            Intent intent = new Intent(this, Horario.class);
            startActivity(intent);
        }
    }
}